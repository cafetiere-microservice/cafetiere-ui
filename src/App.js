import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Landing from './pages/Landing';
import SignIn from './pages/SignIn';
import SignUp from './pages/SignUp';
import Product from './pages/Product';
import ProductDetails from './pages/ProductDetails';
import ContentDetail from './pages/ContentDetail';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Landing} />
        <Route path="/singin" component={SignIn} />
        <Route path="/signup" component={SignUp} />
        <Route path="/product" component={Product} />
        <Route path="/products/:id" component={ProductDetails} />
        <Route path="/post/:id" component={ContentDetail} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
