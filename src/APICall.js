import Api from './axios';

export const authSignUpCall = (data) => {
  return Api.post('/auth/register', data);
};

export const authSignIncall = (data) => {
  return Api.post('/auth/login', data);
};

export const userGetByUsername = (data) => {
  return Api.get(`/user/username/${data}`);
};

export const productGetAll = () => {
  return Api.get('/product/');
};

export const productCreateNew = (data) => {
  return Api.post('/product/', data);
};

export const productGetById = (data) => {
  return Api.get(`/product/${data}`);
};

export const productDelete = (data) => {
  return Api.delete(`/product/${data}`);
};

export const productEdit = (id, data) => {
  return Api.put(`/product/${id}`, data);
};

export const testimonyAllOnProduct = (id) => {
  return Api.get(`/testimonials/list-testimonials/${id}`);
};

export const testimonyCreate = (productId, accountId, data) => {
  return Api.post(`/testimonials/${productId}/${accountId}`, data);
};

export const contentCreate = (id, data) => {
  return Api.post(`/post/accountId=${id}`, data);
};

export const contentGetAll = () => {
  return Api.get('/post/');
};

export const contentGetById = (id) => {
  return Api.get(`/post/postId=${id}`);
};

export const contentDelete = (accId, postId) => {
  return Api.delete(`/post/accountId=${accId}/postId=${postId}`);
};

export const contentEdit = (accId, postId, data) => {
  return Api.put(`/post/accountId=${accId}/postId=${postId}`, data);
};

export const contentGetByTitle = (title) => {
  return Api.get(`/post/containTitle=${title}`);
};

export const commentAdd = (accId, postId, data) => {
  return Api.post(`/comment/accountId=${accId}/postId=${postId}`, data);
};

export const commentDelete = (accId, postId) => {
  return Api.delete(`/comment/accountId=${accId}/commentId=${postId}`);
};

export const notificationSubscribe = (token) => {
  return Api.post(`/notification/${token}`);
}
