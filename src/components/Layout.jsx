import { useState } from 'react';
import { Navbar, Nav, Button, Form, FormControl } from 'react-bootstrap';
import Logo from '../assets/logo.png';
import { Link } from 'react-router-dom';
import { getToken, onMessageListener } from '../firebase.js';
import { ToastContainer, toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import { contentGetByTitle } from '../APICall';

function Layout({ children }) {
  const history = useHistory();
  const [loggedIn, setIsLoggedIn] = useState(localStorage.getItem('token') || null);
  const [search, setSearch] = useState('');

  getToken();

  onMessageListener()
    .then((payload) => {
      toast.info(payload.notification.body);
    })
    .catch(() => toast.error('There is something error with the notification'));

  const logOutHandler = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('accData');
    setIsLoggedIn(null);
    history.push('/');
  };

  const searchHandler = async (e, keyword) => {
    e.preventDefault();
    await contentGetByTitle(keyword)
      .then((res) => {
        history.push(`/post/${res.data[0].idPost}`);
      })
      .catch(() => toast.error('Keyword not found'));
  };

  return (
    <div>
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand>
          <div className="d-flex">
            <Link to="/">
              <img alt="logo" src={Logo} width="60" height="60" />
            </Link>
            <div className="d-flex align-items-center ml-3" style={{ fontFamily: 'Merriweather' }}>
              Cafetiere
            </div>
          </div>
        </Navbar.Brand>
        <Nav className="ml-auto">
          <Nav.Item className="d-flex align-items-center mr-4">
            <Form className="d-flex">
              <FormControl
                type="search"
                placeholder="Search Post Title"
                className="mr-2"
                onChange={(e) => setSearch(e.target.value)}
                aria-label="Search"
              />
              <Button variant="outline-success" onClick={(e) => searchHandler(e, search)}>
                Search
              </Button>
            </Form>
          </Nav.Item>
          <Nav.Item className="d-flex align-items-center mr-4">
            <Link to="/" style={{ textDecoration: 'none', color: 'white' }}>
              Home
            </Link>
          </Nav.Item>
          <Nav.Item className="d-flex align-items-center mr-4">
            <Link to="/product" style={{ textDecoration: 'none', color: 'white' }}>
              Product
            </Link>
          </Nav.Item>
          <Nav.Link>
            {loggedIn ? (
              <Button variant="danger" onClick={() => logOutHandler()}>
                Log Out
              </Button>
            ) : (
              <Link to="/singin">
                <Button variant="primary">Login</Button>
              </Link>
            )}
          </Nav.Link>
        </Nav>
      </Navbar>
      <main>{children}</main>
      <Navbar bg="dark" variant="dark">
        <Nav className="ml-auto">
          <Nav.Link style={{ fontFamily: 'Merriweather' }}>
            Copyright cafetiere group advance programming
          </Nav.Link>
        </Nav>
      </Navbar>
    </div>
  );
}

export default Layout;
