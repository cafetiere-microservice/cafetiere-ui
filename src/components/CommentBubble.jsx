import { AiFillDelete } from 'react-icons/ai';
import { commentDelete } from '../APICall';
import { ToastContainer, toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';

function CommentBubble({ comment, cId }) {
  const history = useHistory();

  const acc = localStorage.getItem('accData') || null;
  const role = acc !== null ? JSON.parse(acc).role : '2';

  const deleteHandler = async (e, role, id) => {
    e.preventDefault();
    await commentDelete(role, id)
      .then(() => {
        toast.success('Succes deleting comment.');
        setTimeout(() => {
          history.go(0);
        }, 2000);
      })
      .catch((e) => {
        toast.error('Failed deleting comment');
        console.log(e);
      });
  };

  return (
    <>
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <div
        style={{ width: '100%', background: 'white', height: '10%', borderRadius: ' 20px' }}
        className="d-flex justify-content-start align-items-center p-2 mb-3"
      >
        {comment}
        {role === '0' && (
          <div
            className="ml-auto"
            style={{ cursor: 'pointer' }}
            onClick={(e) => deleteHandler(e, role, cId)}
          >
            <AiFillDelete />
          </div>
        )}
      </div>
    </>
  );
}

export default CommentBubble;
