import { Card } from 'react-bootstrap';
import Menu from '../assets/menu.jpg';
import { Link } from 'react-router-dom';

function MenuCard({ id, name, description, price }) {
  return (
    <div>
      <Card style={{ width: '18rem', marginRight: '10px', marginTop: '10px', cursor: 'pointer' }}>
        <Card.Img variant="top" src={Menu} />
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Text>{description}</Card.Text>
        </Card.Body>
        <Card.Footer>
          <div className="d-flex">
            <Card.Text>Price: {price}</Card.Text>
            <Card.Text className="ml-auto">
              <Link to={`/products/${id}`}>Details</Link>
            </Card.Text>
          </div>
        </Card.Footer>
      </Card>
    </div>
  );
}

export default MenuCard;
