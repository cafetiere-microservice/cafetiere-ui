import { Button } from 'react-bootstrap';

function LandingHeader() {
  return (
    <div style={{ marginLeft: '30vh' }}>
      <div style={{ color: 'white', fontFamily: 'Merriweather', fontSize: '150px' }}>CAFETIERE</div>
      <div
        style={{
          color: 'white',
          fontFamily: 'Merriweather',
          fontSize: '20px',
          textAlign: 'center',
          marginBottom: '2vh',
        }}
      >
        Buy your coffee here and now!
      </div>
      <div className="d-flex justify-content-center">
        <Button variant="info" size="lg" href="#content">
          Find out more
        </Button>
      </div>
    </div>
  );
}

export default LandingHeader;
