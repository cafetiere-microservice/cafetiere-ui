import ReactLoading from 'react-loading';

function Loading() {
  return (
    <div
      style={{ height: '100vh', width: '100%' }}
      className="d-flex justify-content-center align-items-center bg-dark"
    >
      <ReactLoading type="cylon" />
    </div>
  );
}

export default Loading;
