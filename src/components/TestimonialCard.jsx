import React from 'react';

function TestimonialCard({ title, description, user }) {
  return (
    <div
      className="d-flex flex-column bg-dark p-3"
      style={{
        minWidth: '80%',
        marginBottom: '10px',
        color: 'white',
        borderRadius: '20px',
      }}
    >
      <div style={{ fontSize: '20px', fontWeight: 'bold' }}>{title}</div>
      <div className="d-flex">
        <div style={{ fontSize: '15px' }}>{description}</div>
        <div className="ml-auto">
          created by: <span style={{ color: 'blue' }}>{user}</span>
        </div>
      </div>
    </div>
  );
}

export default TestimonialCard;
