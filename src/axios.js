import axios from 'axios';
const api = axios.create({ baseURL: 'https://cafetiere-api-gateway.herokuapp.com' });
export default api;
