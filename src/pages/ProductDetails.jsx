import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import {
  productGetById,
  productDelete,
  productEdit,
  testimonyCreate,
  testimonyAllOnProduct,
} from '../APICall';
import Layout from '../components/Layout';
import Menu from '../assets/menu.jpg';
import { Button, Modal, Form } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import TestimonialCard from '../components/TestimonialCard';
import Loading from '../components/Loading';

function ProductDetails() {
  const history = useHistory();
  const { id } = useParams();

  const [data, setData] = useState();
  const [finish, setFinish] = useState(false);
  const [testiFinish, setTestiFinish] = useState(false);
  const [show, setShow] = useState(false);
  const [testiShow, setTestiShow] = useState(false);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [quantity, setQuantity] = useState('');
  const [rasa, setRasa] = useState('');
  const [pelayanan, setPelayanan] = useState('');
  const [testiData, setTestiData] = useState([]);

  const datax = localStorage.getItem('accData') || null;
  const acc = datax != null ? JSON.parse(datax) : '2';

  useEffect(() => {
    async function fetchData() {
      await productGetById(String(id)).then((res) => {
        setData(res.data);
        setName(res.data.name);
        setDescription(res.data.description);
        setPrice(res.data.price);
        setQuantity(res.data.quantity);
        setFinish(true);
      });
      await testimonyAllOnProduct(String(id)).then((res) => {
        setTestiData(res.data);
        setTestiFinish(true);
      });
    }
    fetchData();
  }, [id]);

  const deleteHandler = async (e) => {
    e.preventDefault();
    await productDelete(String(id)).then(() => {
      toast.success('Success deleting the product.');
      setTimeout(() => {
        history.push('/product');
      }, 2000);
    });
  };

  const handleSubmit = async (e, name, description, price, quantity) => {
    e.preventDefault();
    await productEdit(String(id), { name, description, price, quantity })
      .then(() => {
        toast.success('Succes editing the product');
        history.go(0);
      })
      .catch(() => toast.error('Fail editing the product'));
  };

  const handleTestimony = async (e, harga, rasa, pelayanan, productId, accountId) => {
    e.preventDefault();
    await testimonyCreate(productId, accountId, { harga, rasa, pelayanan })
      .then(() => {
        toast.success('Success creating testimony');
        setTimeout(() => {
          history.go(0);
        }, 2000);
      })
      .catch(() => {
        toast.error('Failed creating testimony');
      });
  };

  const handleClose = () => setShow(false);
  const handleTestiClose = () => setTestiShow(false);

  if (!finish && !testiFinish) {
    return <Loading />;
  }

  return (
    <Layout>
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <div
        className="bg-dark d-flex flex-column justify-content-center align-items-center"
        style={{ minHeight: '85vh' }}
      >
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Creat New Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Name: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Product Name"
                  value={name}
                  required
                  onChange={(e) => setName(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Description: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Product Description"
                  value={description}
                  required
                  onChange={(e) => setDescription(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Price: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Product Price"
                  value={price}
                  required
                  onChange={(e) => setPrice(Number(e.target.value))}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Quantity: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Product Quantity"
                  value={quantity}
                  required
                  onChange={(e) => setQuantity(Number(e.target.value))}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button
              variant="primary"
              type="submit"
              onClick={(e) => handleSubmit(e, name, description, price, quantity)}
            >
              Update
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal show={testiShow} onHide={handleTestiClose}>
          <Modal.Header closeButton>
            <Modal.Title>Creat New Testimony</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Title: </Form.Label>
                <Form.Control type="text" required onChange={(e) => setRasa(e.target.value)} />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Description: </Form.Label>
                <Form.Control type="text" required onChange={(e) => setPelayanan(e.target.value)} />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleTestiClose}>
              Close
            </Button>
            <Button
              variant="primary"
              type="submit"
              onClick={(e) => handleTestimony(e, price, rasa, pelayanan, id, acc.id)}
            >
              Create
            </Button>
          </Modal.Footer>
        </Modal>
        <div className="d-flex">
          <div
            style={{
              height: '50vh',
              width: '50vh',
              marginRight: '50px',
            }}
          >
            <img
              src={Menu}
              alt="product picture"
              style={{ width: '100%', height: '100%', borderRadius: '20px' }}
            />
          </div>
          <div
            style={{ background: '#ecc042', height: '50vh', width: '90vh', borderRadius: '20px' }}
            className="d-flex flex-column justify-content-center align-items-center"
          >
            <p style={{ fontSize: '50px', fontWeight: 'bold', textAlign: 'center' }}>{data.name}</p>
            <p style={{ fontSize: '20px', textAlign: 'center' }}>{data.description}</p>
            <div className="d-flex">
              <p>
                Price: <span style={{ color: 'green' }}>{data.price}</span>
              </p>
              <p className="ml-5">
                Stock Left: <span style={{ color: 'green' }}>{data.quantity}</span>
              </p>
            </div>
            {acc.role === '0' && (
              <div>
                <Button variant="primary" onClick={setShow}>
                  Edit
                </Button>
                <Button variant="danger" className="ml-3" onClick={(e) => deleteHandler(e)}>
                  Delete
                </Button>
              </div>
            )}
          </div>
        </div>
        <div
          style={{
            minHeight: '20vh',
            width: '145vh',
            background: '#ecc042',
            borderRadius: '20px',
            marginTop: '50px',
          }}
          className="d-flex flex-column justify-content-center align-items-center"
        >
          <p style={{ fontSize: '40px', fontWeight: 'bold' }}>Testimony</p>
          {datax != null && (
            <Button
              variant="primary"
              onClick={() => setTestiShow(true)}
              style={{ marginBottom: '10px' }}
            >
              Create Testimony
            </Button>
          )}
          {testiData.map((testi, index) => (
            <TestimonialCard
              key={index}
              title={testi.testimonials.rasa}
              description={testi.testimonials.pelayanan}
              user={testi.accountName}
            />
          ))}
        </div>
      </div>
    </Layout>
  );
}

export default ProductDetails;
