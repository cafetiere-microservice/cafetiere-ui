import { useState } from 'react';
import Layout from '../components/Layout';
import { Form, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { authSignIncall, userGetByUsername } from '../APICall';
import { ToastContainer, toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import jwt from 'jwt-decode';

function SignIn() {
  const history = useHistory();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const signInHandler = async (e, username, password) => {
    e.preventDefault();
    await authSignIncall({ username, password })
      .then(async (res) => {
        toast.success('Sign In Success!');
        localStorage.setItem('token', res.data);
        await userGetByUsername(jwt(res.data).sub).then((res) => {
          localStorage.setItem('accData', JSON.stringify(res.data));
        });
        setTimeout(() => {
          history.push('/');
        }, 2000);
      })
      .catch(() => toast.error('Sign In Failed, Check Username and Password!'));
  };

  return (
    <Layout>
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <div
        className="d-flex justify-content-center align-items-center bg-dark"
        style={{ height: '85vh' }}
      >
        <Form onSubmit={(e) => signInHandler(e, username, password)}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label style={{ color: 'white' }}>Username: </Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter username"
              required
              onChange={(e) => setUsername(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label style={{ color: 'white' }}>Password: </Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              required
              onChange={(e) => setPassword(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Text style={{ color: 'white' }}>
              Dont have an account?
              <span>
                <Link to="/signup">
                  <a className="text-primary"> Sign Up</a>
                </Link>
              </span>
            </Form.Text>
          </Form.Group>
          <Button
            variant="primary"
            type="submit"
            onClick={(e) => signInHandler(e, username, password)}
          >
            Log In
          </Button>
        </Form>
      </div>
    </Layout>
  );
}

export default SignIn;
