import Layout from '../components/Layout';
import Home from '../assets/home.jpg';
import LandingHeader from '../components/LandingHeader';
import LandingContent from '../components/LandingContent';

function Landing() {
  return (
    <Layout>
      <div
        style={{ backgroundImage: `url(${Home})`, height: '95vh', backgroundSize: 'cover' }}
        className="d-flex justify-content-start align-items-center"
      >
        <LandingHeader />
      </div>
      <div id="content">
        <LandingContent />
      </div>
    </Layout>
  );
}

export default Landing;
