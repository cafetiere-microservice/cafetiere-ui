import firebase from 'firebase/app';
import 'firebase/messaging';
import 'firebase/analytics';
import {notificationSubscribe} from './APICall'

var firebaseConfig = {
  apiKey: 'AIzaSyAMxL-uHempPFIGjmO1PC7oH48WezgqvZM',
  authDomain: 'cafetiere-7d6ef.firebaseapp.com',
  projectId: 'cafetiere-7d6ef',
  storageBucket: 'cafetiere-7d6ef.appspot.com',
  messagingSenderId: '433445931864',
  appId: '1:433445931864:web:d36784556fbdf0b536f32d',
  measurementId: 'G-FBWK8H96WL',
};

firebase.initializeApp(firebaseConfig);
firebase.analytics();

const messaging = firebase.messaging();

export const getToken = () => {
  return messaging
    .getToken({
      vapidKey:
        'BGUiLiTfQjspQy5zxHQVZtqUDEsml5KzV5S9RZbOyo8BddD_mdDbv5GxQQkvQWAtzPii6Agd5eC4qABmSXg-mnY',
    })
    .then(async(currentToken) => {
      if (currentToken) {
        await notificationSubscribe(currentToken)
        .then(() => console.log("Succesfully Subscribe"));
      } else {
        console.log('There is no token');
      }
    })
    .catch((e) => {
      console.log('there is an error: ', e);
    });
};

export const onMessageListener = () =>
  new Promise((resolve) => {
    messaging.onMessage((payload) => {
      resolve(payload);
    });
  });
