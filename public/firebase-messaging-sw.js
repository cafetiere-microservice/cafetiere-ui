importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js');

var firebaseConfig = {
  apiKey: 'AIzaSyAMxL-uHempPFIGjmO1PC7oH48WezgqvZM',
  authDomain: 'cafetiere-7d6ef.firebaseapp.com',
  projectId: 'cafetiere-7d6ef',
  storageBucket: 'cafetiere-7d6ef.appspot.com',
  messagingSenderId: '433445931864',
  appId: '1:433445931864:web:d36784556fbdf0b536f32d',
  measurementId: 'G-FBWK8H96WL',
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

messaging.onBackgroundMessage(function (payload) {
  console.log('Received background message ', payload);

  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
  };

  self.registration.showNotification(notificationTitle, notificationOptions);
});
